@file:JvmName("RestEditAppMain")

package andries.example.restedit

import org.slf4j.LoggerFactory
import org.springframework.beans.BeanWrapperImpl
import org.springframework.beans.NotWritablePropertyException
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest
import java.beans.PropertyEditorSupport
import java.time.LocalDate
import java.util.*
import javax.persistence.*


private val log = LoggerFactory.getLogger(RestEditApp::class.java)

@SpringBootApplication
class RestEditApp {

    @Bean
    @Transactional
    fun initData(repository: PersonRepo) = CommandLineRunner {
        repository.saveAll(listOf(
                Person("Joe", "Blue", "joeB@somecorp.com", LocalDate.of(1982, 12, 10)),
                Person("Joe", "Blow", "joeblow@job.com", LocalDate.of(1992, 6, 1))
        ))
        log.info("Loaded test data!")
    }
}

fun main(args: Array<String>) {
    runApplication<RestEditApp>(*args)
}

@Entity
@Table(indexes = [Index(name = "ix_person_email", columnList = "email", unique = true)])
data class Person(
        @Column(length = 80, nullable = false) var name: String,
        @Column(length = 180, nullable = false) var surname: String,
        @Column(length = 200) var email: String? = null,
        @Column(nullable = false) var dateOfBirth: LocalDate,
        @Id @GeneratedValue var _id: Long? = null
)

@Repository
interface PersonRepo : JpaRepository<Person, Long>

@RestController
@RequestMapping("/persons")
class PersonResource(private val repository: PersonRepo) {

    @PostMapping
    @Transactional
    fun createPerson(@RequestBody person: Person): ResponseEntity<Any?> {
        val newPerson = repository.save(person)
        val newPersonUri = fromCurrentRequest().path("/{id}").build(newPerson._id)
        return ResponseEntity.created(newPersonUri).build()
    }


    @PostMapping("/{id}")
    fun updatePersonById(@PathVariable("id") id: Long, @RequestBody request: MutableMap<Any?, Any?>): ResponseEntity<Any?> {

        request.remove(Person::_id.name) // Important to remove the ID field, as we do not want to update the wrong entity

        val existing = repository.findById(id).value
                ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorDetails.fromRequest("No person with id=$id"))

        try {
            val saved = repository.save(request.update(existing))
            return ResponseEntity.ok(saved)
        } catch (e: NotWritablePropertyException) {
            val possibleMatches = e.possibleMatches?.joinToString(";", prefix = "[", postfix = "]")
            return ResponseEntity.badRequest().body(ErrorDetails.fromRequest(
                    "Property \"${e.propertyName}\" does not exists, " +
                            "or is not writable in Person#${existing._id}. " +
                            "Possible matches are: $possibleMatches"))
        }


    }

    @GetMapping("/{id}")
    fun getPersonById(@PathVariable("id") id: Long): ResponseEntity<Any?> {
        val existing = repository.findById(id).value
                ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorDetails.fromRequest("No person with id=$id"))
        return ResponseEntity.ok(existing)
    }

    @GetMapping
    fun getListOfPersons(
            @RequestParam("page", defaultValue = "0") page: Int = 0,
            @RequestParam("size", defaultValue = "10") size: Int = 10
    ): ResponseEntity<Items<Person>> {
        val results = repository.findAll(PageRequest.of(page, size)).let(::Items)
        return ResponseEntity.ok(results)
    }


}

data class ErrorDetails(val error: String, val uri: String) {
    companion object {
        fun fromRequest(error: String): ErrorDetails {
            return ErrorDetails(
                    uri = ServletUriComponentsBuilder.fromCurrentRequestUri().toUriString(),
                    error = error
            )
        }
    }
}

class Items<T>(val data: List<T>, val _about: Meta) {

    constructor(page: Page<T>) : this(
            page.content,
            Meta(page.number.toLong(), page.size.toLong(), page.isLast, page.isFirst, page.totalElements))

    data class Meta(
            val page: Long,
            val requested: Long,
            val last: Boolean,
            val first: Boolean,
            val total: Long
    )
}

private val <T> Optional<T>.value: T? get() = orElse(null)


private inline fun <reified T : Any> Map<Any?, Any?>.update(dest: T): T {
    return BeanWrapperImpl(true).run {
        registerCustomEditor(LocalDate::class.java, object : PropertyEditorSupport() {
            override fun setAsText(text: String?) {
                value = text?.let { LocalDate.parse(it) }
            }
        })
        setBeanInstance(dest)
        setPropertyValues(this@update)
        wrappedInstance as T
    }
}

