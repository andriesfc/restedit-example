# Example of Simple Edit via Rest

[TOC]

A simple demomstration app to show how to do simple edits using rest API.

TL;DR version: 

1. I used a `Map` to update the model.
2. Made sure the `Person._id` field is not part of the map, otherwise the update will simply insert a new record. 
3. I made sure that update only works on existing entiies

## Interresting Code Snippets

Here is the relavant update code within the `PersonResourc`, using a `Map` which can be passed in either as JSON, or XML:

```kotlin
  @PostMapping("/{id}")
    fun updatePersonById(@PathVariable("id") id: Long, @RequestBody request: MutableMap<Any?, Any?>): ResponseEntity<Any?> {

        request.remove(Person::_id.name) // Important to remove the ID field, as we do not want to update the wrong entity

        val existing = repository.findById(id).value
                ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorDetails.fromRequest("No person with id=$id"))

        try {
            val saved = repository.save(request.update(existing))
            return ResponseEntity.ok(saved)
        } catch (e: NotWritablePropertyException) {
            val possibleMatches = e.possibleMatches?.joinToString(";", prefix = "[", postfix = "]")
            return ResponseEntity.badRequest().body(ErrorDetails.fromRequest(
                    "Property \"${e.propertyName}\" does not exists, " +
                            "or is not writable in Person#${existing._id}. " +
                            "Possible matches are: $possibleMatches"))
        }


    }
```

I'm usin the `BeanWrapperImpl` class to perform the acual update: 

```kotlin
private inline fun <reified T : Any> Map<Any?, Any?>.update(dest: T): T {
    return BeanWrapperImpl(true).run {
        registerCustomEditor(LocalDate::class.java, object : PropertyEditorSupport() {
            override fun setAsText(text: String?) {
                value = text?.let { LocalDate.parse(it) }
            }
        })
        setBeanInstance(dest)
        setPropertyValues(this@update)
        wrappedInstance as T
    }
}
```

### Loading of Initial Data

```kotlin
@Bean
@Transactional
fun initData(repository: PersonRepo) = CommandLineRunner {
    repository.saveAll(listOf(
            Person("Joe", "Blue", "joeB@somecorp.com", LocalDate.of(1982, 12, 10)),
            Person("Joe", "Blow", "joeblow@job.com", LocalDate.of(1992, 6, 1))
    ))
    log.info("Loaded test data!")
}
```

## Map of the Code

| Artitact                                                                  | Responsibilties                             |
| ------------------------------------------------------------------------- | ------------------------------------------- |
| `class RestEditApp`                                                       | Spring boot strap.                          |
| `fun main(...)`                                                           | The main method to launch the application   |
| ` data class Person`                                                      | The person entity                           |
| ` interface PersonRepo`                                                   | A spring JPA repository to save persons to. |
| `class PersonResource(private val repository: PersonRepo) {`              | A person resource backed by a JPA data store. |                   | `data class ErrorDetails(val error: String, val uri: String)`             | A simple error report response entity.      |
| `class Items<T>(val data: List<T>, val _about: Meta)`                     | A response enity for items with some meta data. |
| `val <T> Optional<T>.value: T? get() = orElse(null)`                      | An extension property to return null from an optional. |
| `private inline fun <reified T : Any> Map<Any?, Any?>.update(dest: T): T` | An extension function to update bean via a map. |

## Issues to Consider

### Issues with using a `Map<Any?,Any?>`

1. A Map may work with simple entities, but it does mean we loose the ability to update only specific entities.

## Building and running

1. Execute the following command to build

```bash
./gradlew ass
```

(fow windows):

```
gradlew.bat ass
```
2. Execute with: 

```
java -jar build/libs/rest-edit-example-0.0.1-SNAPSHOT.jar
```

